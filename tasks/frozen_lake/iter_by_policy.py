import gym
import numpy as np

"""
Алгоритм итерации по стратегии
"""

env = gym.make('FrozenLake-v1')
env = env.unwrapped
nA = env.action_space.n
nS = env.observation_space.n
V = np.zeros(nS)
policy = np.zeros(nS)

"""
0: LEFT
1: DOWN
2: RIGHT
3: UP
"""
arrows = {
    0: '\u2190',
    1: '\u2193',
    2: '\u2192',
    3: '\u2191'
}
#print(env.P)


def eval_state_action(V, s, a, gamma=0.99):
    return np.sum([p * (rew + gamma*V[next_s]) for p, next_s, rew, _ in env.P[s][a]])


def policy_evaluation(V, policy, eps=0.0001):
    """
    Оценка стратегии
    :param V:
    :param policy:
    :param eps:
    :return:
    """
    while True:
        delta = 0
        for s in range(nS):
            old_v = V[s]
            V[s] = eval_state_action(V, s, policy[s])  # оценка действия
            delta = max(delta, np.abs(old_v - V[s]))
        if delta < eps:
            break


def policy_improvement(V, policy):
    policy_stable = True
    for s in range(nS):
        old_a = policy[s]
        policy[s] = np.argmax([eval_state_action(V, s, a) for a in range(nA)])
        if old_a != policy[s]:
            policy_stable = False
    return policy_stable


def run_episodes(env, V, policy, num_games=100):
    tot_rew = 0
    state = env.reset()[0]
    for _ in range(num_games):
        done = False
        while not done:
            next_state, reward, done, _, _ = env.step(policy[state])
            state = next_state
            tot_rew += reward
            if done:
                state = env.reset()[0]
        #env.render()
    print('Выиграно %i из %i игр!'%(tot_rew, num_games))


def print_policy(policy):
    convert = lambda x: arrows[x]
    for p in policy:
        print(' '.join(list(map(convert, [int(x) for x in p]))))


policy_stable = False
it = 0
while not policy_stable:
    policy_evaluation(V, policy)  # оценка стратегии, под текущую стратегию он обновит функцию ценности состояний V
    policy_stable = policy_improvement(V, policy)  # улучшение стратегии
    it += 1

print('Сошелся после %i итераций по стратегиям'%(it))
run_episodes(env, V, policy)
#print(V.reshape((4, 4)))
real_policy = policy.reshape((4, 4))

print_policy(real_policy)

print('Done')
