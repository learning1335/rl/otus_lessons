# Символы стрелок
arrow_up = '\u2191'
arrow_down = '\u2193'
arrow_left = '\u2190'
arrow_right = '\u2192'
arrow_up_left = '\u2196'
arrow_up_right = '\u2197'
arrow_down_left = '\u2199'
arrow_down_right = '\u2198'

# Вывод символов стрелок
print("Стрелка вверх:", arrow_up)
print("Стрелка вниз:", arrow_down)
print("Стрелка влево:", arrow_left)
print("Стрелка вправо:", arrow_right)
print("Диагональная стрелка вверх-влево:", arrow_up_left)
print("Диагональная стрелка вверх-вправо:", arrow_up_right)
print("Диагональная стрелка вниз-влево:", arrow_down_left)
print("Диагональная стрелка вниз-вправо:", arrow_down_right)
