import gym
import numpy as np

"""
Алгоритм итерации по ценности
"""

env = gym.make('FrozenLake-v1')
env = env.unwrapped
nA = env.action_space.n
nS = env.observation_space.n
V = np.zeros(nS)
policy = np.zeros(nS)

"""
0: LEFT
1: DOWN
2: RIGHT
3: UP
"""
arrows = {
    0: '\u2190',
    1: '\u2193',
    2: '\u2192',
    3: '\u2191'
}
#print(env.P)


def eval_state_action(V, s, a, gamma=0.99):
    """
    Оценка ценности пары состояние-действие
    :param V:
    :param s:
    :param a:
    :param gamma:
    :return:
    """
    return np.sum([p * (rew + gamma*V[next_s]) for p, next_s, rew, _ in env.P[s][a]])


def value_iteration(eps=0.0001):
    V = np.zeros(nS)
    it = 0
    while True:
        delta = 0
        # обновить ценность каждого состояния
        for s in range(nS):
            old_v = V[s]
            # формула (3.10)
            V[s] = np.max([eval_state_action(V, s, a) for a in range(nA)])
            delta = max(delta, np.abs(old_v - V[s]))
        # если стабилизировалась, выйти из цикла
        if delta < eps:
            break
        else:
            print('Итерация:', it, ' дельта:', np.round(delta,5))
        it += 1
    return V


def run_episodes(env, V, num_games=100):
    tot_rew = 0
    state = env.reset()[0]
    for _ in range(num_games):
        done = False
        while not done:
            # выбрать наилучшее действие, пользуясь функцией ценности
            # формула (3.11)
            action = np.argmax([eval_state_action(V, state, a) for a in range(nA)])
            next_state, reward, done, _, _ = env.step(action)
            state = next_state
            tot_rew += reward
            if done:
                state = env.reset()[0]
    print('Выиграно %i из %i игр!' % (tot_rew, num_games))


def print_policy(policy):
    convert = lambda x: arrows[x]
    for p in policy:
        print(' '.join(list(map(convert, [int(x) for x in p]))))


V = value_iteration(eps=0.0001)
run_episodes(env, V, 100)
print(V.reshape((4, 4)))


print('Done')
