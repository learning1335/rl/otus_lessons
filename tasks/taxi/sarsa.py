import gym
import numpy as np


def greedy(Q, s):
    return np.argmax(Q[s])


def eps_greedy(Q, s, eps=0.1):
    if np.random.uniform(0, 1) < eps:
        # Выбрать случайное действие
        return np.random.randint(Q.shape[1])
    else:
        # Выбрать жадное действие
        return greedy(Q, s)


def SARSA(env, lr=0.01, num_episodes=10000, eps=0.3, gamma=0.95, eps_decay=0.00005):
    nA = env.action_space.n
    nS = env.observation_space.n
    test_rewards = []
    Q = np.zeros((nS, nA))
    games_reward = []

    for ep in range(num_episodes):
        state = env.reset()
        state = state[0]
        done = False
        tot_rew = 0

        if eps > 0.01:
            eps -= eps_decay

        action = eps_greedy(Q, state, eps)

        while not done:
            next_state, rew, done, _, info = env.step(action)  # один шаг взаимодействия со средой
            next_action = eps_greedy(Q, next_state, eps)
            Q[state][action] = Q[state][action] + lr * (rew +
                                                        gamma * Q[next_state][next_action] - Q[state][action])  # формула (4.5)
            state = next_state
            action = next_action
            tot_rew += rew
            if done:
                games_reward.append(tot_rew)

        # if (ep > 0) and ((ep % 100) == 0):
        #     test_rew = run_episodes(env, Q, 100, to_print=True)
        #     print("Эпизод:{:5d} Эпс:{:2.4f} Вознагр:{:2.4f}".format(ep, eps, test_rew))
        #     test_rewards.append(test_rew)

    return Q


def run_episodes(env, Q, num_episodes=100, to_print=False):
    tot_rew = []
    state = env.reset()
    #env.render()
    state = state[0]
    for _ in range(num_episodes):
        done = False
        game_rew = 0
        while not done:
            next_state, rew, done, timeout, info = env.step(greedy(Q, state))
            state = next_state
            game_rew += rew
            if done:
                state = env.reset()
                state = state[0]
                tot_rew.append(game_rew)
    if to_print:
        print('Средний счет: %.3f из %i игр!' % (np.mean(tot_rew), num_episodes))
    else:
        return np.mean(tot_rew)


if __name__ == '__main__':
    env = gym.make('Taxi-v3')
    env.reset()
    Q = SARSA(env, lr=.1, num_episodes=5000, eps=0.4, gamma=0.95, eps_decay=0.001)
    print(Q)
    run_episodes(env, Q, num_episodes=100, to_print=True)
    print('Done')
