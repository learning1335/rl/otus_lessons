import gym

# создать окружающую среду
env = gym.make("CartPole-v1")
print(env.observation_space)
print(env.action_space)

print(env.observation_space.low)
print(env.observation_space.high)

# привести среду в исходное состояние перед началом
env.reset()

# повторять 10 раз
for i in range(10):
    # предпринять случайное действие
    action = env.action_space.sample()
    print(action)

    observation, reward, terminated, truncated, info = env.step(action)

    print(observation, reward, terminated, truncated, info)
    # нарисовать состояние игры

    env.render()

# закрыть окружающую среду
env.close()
